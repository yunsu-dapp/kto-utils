# kto-utils

#### 介绍
korthor链，js工具库




#### 安装教程

npm kto-utils

yarn add kto-utils

#### 使用说明

```
var KtoUtils=require("kto-utils");
let res=ktoUtils.keyPair();
console.log('生成地址',res)
//--------
生成地址 {
  ktoAddress: 'Kto55XQpzF5fXQMtW8HNS6ufMDfYTM1qSeHG8bVywAjgqU3',
  privateKry: '2o3rn2XGWutEZqLpgjWUFGjN2n1z4nKrPwt6iiUCcNtfxs87mztxYAe186j2R5RvEG9M8F9mFRdgkaLMANfb3wUK'
} 
```

#### 参与贡献

1. caster



<!-- 撞击块 -->