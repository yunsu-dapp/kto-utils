var ktoUtils = require("../index");

class Test {
    sign() {
        var key = "2XRyFnfbXp4pQMUS2pbxmLprdo7sym4C8cWWGewnsvmPvGF5RkXMFi5rLeRaDD2eG43PB5zX2w3Xi8hft54PXmbN";
        key = ktoUtils.decode58(key);
        // hex 格式 msg，前缀不能携带0x
        var msg = "80c73764f9c50daadc3c466a2cbc6e603132b2309ed9ec48599aa9f756d0870b";
        let sign = ktoUtils.sign(Buffer.from(msg, 'hex'), Buffer.from(key, 'hex'));
        console.log('签名', sign, ktoUtils.buf2hex(sign.signature))
    }
    base58() {
        let string = "Kto8TPGEstvarfvoyX8BmDRey5DLsWLNzsU9ziskaUSwPqh";
        let decode = ktoUtils.decode58(string)
        console.log("解码：", decode, "长度：", decode.length)

        let hex = ktoUtils.buf2hex(decode);
        console.log("\n", "buffer 转 hex 结果", hex, hex.length);
    }
    verify() {
        let hex = "e45b89f0421f286733c94bfdf734b0faf0303ecce77821d88443fc74d3b490c53a212acbe46c9046fb9006c57a3c32f9f6866d7227c67f19c59f44b62254420a";
        let msg = "80c73764f9c50daadc3c466a2cbc6e603132b2309ed9ec48599aa9f756d0870b";
        let key = "J9RgmB518exJhR98xqYgU94xzdaVvDM9RZJhKA8XAvqn";
        key = ktoUtils.decode58(key)
        console.log('公钥长度', key, key.length)
        let res = ktoUtils.verify(
            Buffer.from(msg, 'hex'),
            Buffer.from(hex, 'hex'),
            Buffer.from(key, 'hex'),

        );
        console.log('验签结果', res)
    }
    ecsign() {
        let publicKey = "8TPGEstvarfvoyX8BmDRey5DLsWLNzsU9ziskaUSwPqh";
        let privateKey = "Zj9ZyajAb7tuvYYDFqxVenZopj8fbu64x4eKaikEFYVdqNbs8zpfKGogfzwA4Kgw7FZEKass5LxpAb3avi1ffzo";
        let msg = "0xcf67674055e346ef69e7d50b2a109766131d3c145ac4aeff092ba8f9269daff1";
        privateKey = ktoUtils.decode58(privateKey);
        publicKey = ktoUtils.decode58(publicKey);
        privateKey = ktoUtils.buf2hex(privateKey)
        publicKey = ktoUtils.buf2hex(publicKey)

        console.log('签名传递参数', {
            msg,
            buffer_msg: Buffer.from(msg),
            privateKey,
            buffer_privateKey: Buffer.from(privateKey)
        })
        let sign = ktoUtils.sign(Buffer.from(msg), privateKey);

        let r = sign.signature.slice(0, 32);
        let s = sign.signature.slice(32, 64)

        console.log("签名结果",
            {
                sign: sign,
                hex_sign: ktoUtils.buf2hex(sign.signature),
                r: ktoUtils.buf2hex(r),
                s: ktoUtils.buf2hex(s)
            })
        console.log(Buffer.from('0'),
            Buffer.from(sign.signature),
            Buffer.from(publicKey))
        let res = ktoUtils.verify(
            Buffer.from('0x'),
            Buffer.from(sign.signature),
            Buffer.from(publicKey),
        );
        console.log('验签结果', res)
    }
    /**
     * @dev 创建kto公钥（32字节），私钥（64字节）对
     * {
        ktoAddress: 'KtoJ9RgmB518exJhR98xqYgU94xzdaVvDM9RZJhKA8XAvqn',
        privateKry: '2XRyFnfbXp4pQMUS2pbxmLprdo7sym4C8cWWGewnsvmPvGF5RkXMFi5rLeRaDD2eG43PB5zX2w3Xi8hft54PXmbN'
       }
     */
    keyPair() {
        let res = ktoUtils.keyPair();
        console.log('生成地址', res)
    }
    fromSecretKey() {
        let secretKey = "2XRyFnfbXp4pQMUS2pbxmLprdo7sym4C8cWWGewnsvmPvGF5RkXMFi5rLeRaDD2eG43PB5zX2w3Xi8hft54PXmbN";
        secretKey = ktoUtils.decode58(secretKey)
        console.log('hex', secretKey)
        let ret = ktoUtils.fromSecretKey(Buffer.from(secretKey));
        console.log(ret)
    }
}
var test = new Test();
test.keyPair();

