"use strict";
var ed2519 = require("tweetnacl");
ed2519.util = require("tweetnacl-util");
var base58 = require("./lab/base58");
var buf2hex = require("./lab/buf2hex")

// ------------------------------------------------------- util

/**
 * buffer 数据编码为 base58
 * @param {Buffer} buffer 
 * @returns {String}
 */
exports.encode58 = function (buffer) {
    return base58.decode58(buffer);
}
/**
 * base58 数据解码为 buffer
 * @param {String} string 
 * @returns {Buffer}
 */
exports.decode58 = function (string) {
    return base58.decode58(string);
}
/**
 * buffer 转 hex
 * @param {*} buffer 
 * @returns {String}
 */
exports.buf2hex = function (buffer) {
    return buf2hex.buf2hex(buffer)
}
// ------------------------------------------------------- account

/**
 * 生成kto密钥对，公钥（32字节），私钥（64字节）
 * @returns {Object}
 * ktoAddress kto 地址
 * privateKry kto 私钥
 */
exports.keyPair = function () {
    let ret = ed2519.sign.keyPair();
    let publicKey = base58.encode58(ret.publicKey);
    let secretKey = base58.encode58(ret.secretKey);
    while (publicKey.length != 44 || secretKey.length != 87) {
        ret = ed2519.sign.keyPair();
        publicKey = base58.encode58(ret.publicKey);
        secretKey = base58.encode58(ret.secretKey);
    }
    return {
        ktoAddress: 'Kto' + publicKey,
        privateKry: secretKey
    }
}



/**
 * 从私钥还原出kto公钥对
 * @param {*} secretKey 
 * @returns 
 */
exports.fromSecretKey = function (secretKey) {
    let ret = ed2519.sign.keyPair.fromSecretKey(secretKey);
    return {
        publicKey: 'Kto' + base58.encode58(ret.publicKey),
        privateKry: base58.encode58(ret.secretKey)
    }
}
// ------------------------------------------------------- sign

/**
 * @dev ED25519 椭圆曲线签名
 */
exports.ecsign = function (msgHash, privateKey, chainId) {
    var sig = this.sign(msgHash, privateKey);
    var recovery = sig.recovery;
    var ret = {
        r: sig.signature.slice(0, 32),
        s: sig.signature.slice(32, 64),
        v: chainId ? recovery + (chainId * 2 + 35) : recovery + 27,
    }
    return ret;
}
/**
 * Create an ED2519 signature. Always return low-S signature.
 * @method sign
 * @param {Buffer} message
 * @param {Buffer} privateKey
 * @param {Object} options
 * @return {Buffer}
 */
exports.sign = function (message, privateKey, options) {
    if (options === null) {
        throw new TypeError('options should be an Object');
    }
    var sig = ed2519.sign.detached(Uint8Array.from(message), Uint8Array.from(privateKey));
    return {
        signature: Buffer.from(sig),
        recovery: 1,
    }
}

/**
 * 验证签名
 * @param {*} buffer 
 * @returns {Boolean}
 */
exports.verify = function (msg, sign, key) {
    return ed2519.sign.detached.verify(msg, sign, key)
}

